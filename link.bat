@echo off
if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin & exit /b)

cd C:\RosaeNLG\kb\rosaenlg-parallel-poc
mklink /D node_modules\rosaenlg c:\Freenlg\rosaenlg\packages\rosaenlg

pause
